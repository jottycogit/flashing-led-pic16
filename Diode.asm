		LIST		p=16F84A
		__CONFIG	03FF1H
		
STATUS	equ			03h
PORTB	equ			06h
TRISB	equ			06h

Reg_1	equ			0Ch
Reg_2	equ			0Dh
Reg_3	equ			0Eh

		org 0
		bsf			STATUS,5
		clrf		TRISB
		bcf			STATUS,5
start_delaying	
		bsf			PORTB,4
		call 		delay
		bcf 		PORTB,4
		call 		delay
		goto		start_delaying

delay	movlw		.169
		movwf		Reg_1
		movlw		.69
		movwf		Reg_2
		movlw		.2
		movwf		Reg_3
		decfsz		Reg_1,F
		goto		$-1
		decfsz		Reg_2,F
		goto		$-3
		decfsz		Reg_3,F
		goto		$-5
		nop
		nop
		return

		end